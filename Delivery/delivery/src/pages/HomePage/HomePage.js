import React from "react";
import Item from "../../components/Item";
import { useEffect } from "react";
import { useSelector, useDispatch, shallowEqual } from "react-redux";
import { getData } from "../../appStore/actionCreators/actionCreator";

const HomePage = () => {
  const { items } = useSelector(({ items }) => items, shallowEqual);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(getData());
  }, []);

  return (
    <div>
      <h1 style={{ color: "white" }}>HOME</h1>
      <div
        style={{ display: "flex", flexWrap: "wrap", justifyContent: "center " }}
      >
        {items.map((element) => {
          return (
            <Item
              key={element.id}
              color={element.color}
              name={element.name}
              url={element.url}
              price={element.price}
              id={element.id}
              isFavorite={element.isFavorite}
              //   addToFav={addToFav}
            />
          );
        })}
      </div>
    </div>
  );
};

export default HomePage;
