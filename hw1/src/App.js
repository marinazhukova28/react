import React from "react";
import Header from "./components/Header/Header";

import HomePage from "./pages/HomePage";
import { Switch, Route } from "react-router-dom";

const App = (props) => {
  return (
    <div className="App">
      <Header />

      <Switch>
        <Route exact path="/" component={HomePage} />
      </Switch>
    </div>
  );
};

export default App;
