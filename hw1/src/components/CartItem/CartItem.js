import React from 'react';
import styles from './CartItem.module.scss';

const CartItem = (props) => {
    const { name, count } = props;

    return (
        <div className={styles.root}>
            <div>
                <span>{name}</span>
            </div>
            <span>{count}</span>
        </div>
    )
}

export default CartItem;