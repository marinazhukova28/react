async function getProducts() {
  let responseURL = await fetch("products.json");

  let products = await responseURL.json();

  // localStorage.setItem('Products Array', JSON.stringify(products))
  return products;
}

export default getProducts;
