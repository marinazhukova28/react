import React from "react";
import Modal from "../Modal/Modal";
import App from "../../App";
const Button = ({ ...props }) => {

return(
    
        <div className="btn__wrapper" >
           <button onClick={props.onBtn}        
                   style = {{...props.style}}
           >
                   {props.text}
        
            </button>
        </div>
    
)
}

export default Button
