import React from "react" 
import "./Modal.scss"

const Modal = props =>{


    return(
        
        <div className={`modal__wrapper ${props.isOpened ? 'open': 'close'}`} style ={{...props.style}}
        >
            <div className ='modal__body'>
                <div className ='modal__header'>
                <h2>{props.header}</h2>
                <div className = 'modal__close' onClick={props.onModalClose}>
                ×
                </div>
                </div>

                <div className = 'modal__massage'>
                    {props.text}
                </div>

                <div className="modal__ftr">
                    <div className="modal__actions">
                        {props.actions1}
                        {props.actions2}
                    </div>
                
                </div>
            </div>
            
        </div>
    )
}

export default Modal