import React, {useEffect, useState} from 'react';
import styles from './Cart.module.scss';
import CartItem from "../CartItem";

const Cart = (props) => {
    const { items } = props;
    const [sum, setSum] = useState(0);

    useEffect(()=> {
        let sum = 0;
        items.forEach(e => sum += e.price * e.count);
        setSum(sum);
    }, [items])

    return (
        <section className={styles.root}>
            <h2>CART</h2>
            <div className={styles.items}>
                {items && items.map(item => <CartItem {...item} />)}
            </div>
            <p>Price: {sum}$</p>
        </section>
    )
}

export default Cart;