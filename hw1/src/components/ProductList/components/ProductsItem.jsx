import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { useState } from "react";
import Button from "../../Button/Button";
import Modal from "../../Modal/Modal";
import { ReactComponent as StarFav } from "../../svg/starFav.svg";
import styles from "./ProductsItem.module.scss";

const ProductsItem = (props) => {
  const { name, article, color, price, url } = props.product;

  const isFavLocalStorage = !!localStorage.getItem(name);
  const [isFavorite, setFavorite] = useState(isFavLocalStorage);
  
  const isCartLocalStorage = !!localStorage.getItem(price);
  const [isCart, setCart] = useState(isCartLocalStorage);

  const [modal, setModal] = useState({
    modal1: false,
    modalBuy: false,
  });

  // const favoriteProducts = JSON.parse(window.localStorage.getItem("products"));
  return (
    <li className={styles.item}>
      <div>
        <img className={styles.img} src={url} alt={name}></img>
      </div>
      <div className={styles.title}>{name}</div>
      <div className={styles.price}>{price}</div>
      <div className={styles.color}>
        Color: {color}, art: {article}
      </div>

      <div className={styles.otherInfo}>
        <div
          className={styles.favourites}
          onClick={() => {
            if (isFavorite) {
              localStorage.removeItem(name);
            } else {
              localStorage.setItem(name, JSON.stringify(props.product));
            }

            setFavorite(!isFavorite);
          }}
        >
          <StarFav style={{ fill: isFavorite ? "#DC780B" : "#101010" }} />
        </div>

        <Button
          text="Add to cart"
          onBtn={() =>{ 
            setModal({
              ...modal,
              modalBuy: true,
            })
          }}
          style={{ backgroundColor: "#DC780B" }}
        />
      </div>

      <React.Fragment>
        <Modal
          header={"Your dishes has been added!"}
          isOpened={modal.modalBuy}
          onModalClose={() => setModal({ ...modal, modalBuy: false })}
          text={
            "Super! The food is almost there! Shall we continue the selection or proceed with the delivery?"
          }
          
          actions1={<Button text={"Delivery inform"}  style={{backgroundColor: "#EB8E78"}} />}
          actions2={<Button 
            text={"Yes, add to cart"}
            style={{backgroundColor: "#EB8E78"}}           
            onBtn={() =>{
                if (isCart) {
                  localStorage.removeItem(price);
                } else {
                  localStorage.setItem( price, JSON.stringify(props.product));
                }
                setCart(!isCart);
              }}
              
            />}>
         
        
        </Modal>
      </React.Fragment>
    </li>
  );
};

export default ProductsItem;
