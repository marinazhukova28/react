import React from "react";
import ProductsItem from "./components/ProductsItem";
import styles from "./ProductsList.module.scss";

const ProductList = (props) => {
  

  const{products,addItem } = props;

  return (
    <div className={styles.ourMenu}>
      <h2 className={styles.title}>Menu</h2>
      <ol className={styles.productList}>
        {products.map((product, index) => (
          <ProductsItem  key={index} product={product} addItem={addItem} />
        ))}
      </ol>
    </div>
  );
}

export default ProductList;
