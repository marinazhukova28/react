import React from 'react'
import { AiOutlineStar } from 'react-icons/ai'
import { BsCart4} from "react-icons/bs"
import { Link } from 'react-router-dom'
import "./Header.scss"

const Header = (props) => {
	return (
		<header>
			
			<nav className="main-nav">
				<ul className="nav-list">

                <h1 className="logo-title">
				<Link to="/" className="nav-item-link">
					<img
						className="logo-img"
						src="https://fiji-loungebar.com.ua/wp-content/themes/bootstrap-canvas-wp/images/logo.png"
					/>
				</Link>
			</h1>

					<li className="nav-item">
						<Link to="/" className="nav-item-link">
							Home
						</Link>
					</li>				
					
					<li className="nav-item">
						<Link to="/favorites" className="nav-item-link">
							<AiOutlineStar className="star-icon" />
						</Link>
					</li>

					<li className="nav-item">
						<Link to="/basket" className="nav-item-link">
							<BsCart4 className="star-icon" />
						</Link>
					</li>
				</ul>
			</nav>
		</header>
	)
}

export default Header
